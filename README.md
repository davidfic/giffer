giffer
======

A gif storage system 


Problem
=======
You're chatting with a friend and they type something that makes you want to respond with an appropriate gif for the moment.

The problem is that you then have to go to Google and search for the correct gif. Copy the URL, possibly put it throw a 
link shortner and then paste it into your conversation. By the time you've done this the conversation has moved or and the 
timing is wrong. 

Solution
=======
Use Giffer. With this system you store your gifs in one nice place broken up into neat meaningful categories and when you 
have need of a gif you can easily retreive it from your gif library far faster then poking around on the web. 
