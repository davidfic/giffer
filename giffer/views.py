from giffer import giffer
from models import Gif
from flask import Flask, render_template, redirect, request
from forms import MyForm
from db import categories

def get_categories_from_db():
	# categories = conn.categories
	return categories.find({},{'categories':1})



cats = []
gif  = Gif('test1', "catg2")
@giffer.route('/test-cat')
def cat_test():
	cats = gif.list_categories()
	return render_template('test.html', cats=cats)	

@giffer.route('/')
@giffer.route('/index', methods=('GET', 'POST'))
def index():
	form = MyForm()
	if form.validate_on_submit():
		return redirect('/submit')
	return render_template('index.html', form=form)

@giffer.route('/submit', methods=('GET', 'POST'))
def submit():
	if request.method == 'POST':
		# print "name is", request.form['name']
		cats.append(request.form['name'])

		# categories = conn.categories
		categories.insert({'categories': request.form['name']})
		dbcats = get_categories_from_db()
		print type(dbcats)

		for cat in dbcats:
			print "db:", cat['categories']
		# categories.insert({'test1': 33})

		# print "label is", request.form['label']
	return render_template('submit.html',form=dbcats)