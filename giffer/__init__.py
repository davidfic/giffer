from flask import Flask
from pymongo import MongoClient




connection = MongoClient(<<url>>,<<port>>)
db = connection["giffer"]
 # MongoLab has user authentication
db.authenticate(<<username>>,<<password>>)


giffer = Flask(__name__)
giffer.secret_key = <<super secret>>

from giffer import views
