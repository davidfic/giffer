from flask_wtf import Form
from wtforms import TextField
from wtforms.validators import DataRequired

class MyForm(Form):
    category = TextField('category', validators=[DataRequired()])
    url = TextField('url', validators=[DataRequired()])

