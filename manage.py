from flask.ext.script import Manager
from giffer import giffer

manager = Manager(giffer)


@manager.command
def hello():
    print "hello"

if __name__ == "__main__":
    manager.run()
